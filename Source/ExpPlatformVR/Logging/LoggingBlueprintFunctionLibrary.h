#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FileHelper.h"
#include "Paths.h"
#include "UnrealString.h"
#include "LoggingBlueprintFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class EXPPLATFORMVR_API ULoggingBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
	UFUNCTION(BlueprintCallable, Category = "CustLog")
		static bool CreateFile(FString FileName);

	UFUNCTION(BlueprintCallable, Category = "CustLog")
		static bool FileSaveString(FString SaveText, FString FileName);

	UFUNCTION(BlueprintCallable, Category = "CustLog")
		static bool FileSaveStringOverwrite(FString SaveText, FString FileName);

	UFUNCTION(BlueprintCallable, Category = "CustLog")
		static bool FileLoadString(FString FileName, FString& SaveText);
};
