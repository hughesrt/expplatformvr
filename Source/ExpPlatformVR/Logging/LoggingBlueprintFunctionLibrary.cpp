#include "LoggingBlueprintFunctionLibrary.h"

bool ULoggingBlueprintFunctionLibrary::CreateFile(FString FileName) {
	return true;
}

bool ULoggingBlueprintFunctionLibrary::FileSaveString(FString SaveText, FString FileName)
{
	return FFileHelper::SaveStringToFile(SaveText, *(FPaths::GameDir() + FileName), FFileHelper::EEncodingOptions::AutoDetect, &IFileManager::Get(), FILEWRITE_Append);
}

bool ULoggingBlueprintFunctionLibrary::FileSaveStringOverwrite(FString SaveText, FString FileName)
{
	return FFileHelper::SaveStringToFile(SaveText, *(FPaths::GameDir() + FileName), FFileHelper::EEncodingOptions::AutoDetect, &IFileManager::Get(), FILEWRITE_NoReplaceExisting);
}

bool ULoggingBlueprintFunctionLibrary::FileLoadString(FString FileName, FString& SaveText)
{
	return FFileHelper::LoadFileToString(SaveText, *(FPaths::GameDir() + FileName));
}