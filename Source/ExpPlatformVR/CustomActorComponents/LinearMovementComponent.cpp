#include "LinearMovementComponent.h"
#include "CustomMath/CustomMath.h"


// Sets default values for this component's properties
ULinearMovementComponent::ULinearMovementComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	timeCurrent = 0;

	positionA.Set(200.0f, 50.0f, 360.0f);
	positionB.Set(200.0f, -50.0f, 360.0f);
	positionCurrent = positionA;

	arrived = false;
}


// Called when the game starts
void ULinearMovementComponent::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void ULinearMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	timeCurrent += (DeltaTime / 2);

	if (((positionCurrent - positionB).Size() > 1.f))
	{
		positionCurrent = FMath::Lerp(positionA, positionB, timeCurrent);
	}
	else
	{
		arrived = true;
		SetComponentTickEnabled(false);
	}

	AActor* Parent = GetOwner();
	if (Parent)
	{
		Parent->SetActorLocation(positionCurrent);
	}
}

void ULinearMovementComponent::ResetComponent()
{
	timeCurrent = 0;

	positionA.Set(200.0f, 50.0f, 360.0f);
	positionB.Set(200.0f, -50.0f, 360.0f);
	positionCurrent = positionA;

	arrived = false;
	SetComponentTickEnabled(true);
}