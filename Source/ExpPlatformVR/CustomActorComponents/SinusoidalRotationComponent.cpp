#include "SinusoidalRotationComponent.h"
#include "CustomMath/CustomMath.h"
#include <Windows.h>

//UNREAL units are in cm, rather than OpenGL's m
#define FREQ_SCALE 1
#define TRANSFORM_SCALE 0.5//5 

USinusoidalRotationComponent::USinusoidalRotationComponent() :
	frequencyArray{ 0.09*FREQ_SCALE, 0.165*FREQ_SCALE, 0.195*FREQ_SCALE, 0.375*FREQ_SCALE, 0.495*FREQ_SCALE }
{
	PrimaryComponentTick.bCanEverTick = true;

	for (int i = 0; i < 5; i++) {
		randomPhase[i].Set(
			RandFloat(2 * PI_F),
			RandFloat(2 * PI_F),
			RandFloat(2 * PI_F));
	}
	transformScale = 1.0f;
	targetRotation = FRotator(0, 0, 0);
	frequencyArray[0] = 0.09 * 50;
	frequencyArray[1] = 0.165 * 50;
	frequencyArray[2] = 0.195 * 50;
	frequencyArray[3] = 0.375 * 50;
	frequencyArray[4] = 0.495 * 50;
}

// Called when the game starts
void USinusoidalRotationComponent::BeginPlay()
{
	Super::BeginPlay();
	timeStart = GetWorld()->GetRealTimeSeconds();
	timeCurrent = 0;
}

// Called every frame
void USinusoidalRotationComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	timeCurrent = GetWorld()->GetRealTimeSeconds() - timeStart;

	targetRotation = FRotator(0, 0, 0);
	for (int i = 0; i < 5; i++) {
		targetRotation.Pitch += (float)(sin(timeCurrent * 2 * PI_F * frequencyArray[i] + randomPhase[i].X)) * transformScale;
		targetRotation.Roll += (float)(sin(timeCurrent * 2 * PI_F * frequencyArray[i] + randomPhase[i].Y)) * transformScale;
		targetRotation.Yaw += (float)(sin(timeCurrent * 2 * PI_F * frequencyArray[i] + randomPhase[i].Z)) * transformScale;
	}

	AActor* Parent = GetOwner();
	if (Parent && active)
	{
		Parent->SetActorRotation(Parent->GetActorRotation() + targetRotation);
	}
}

