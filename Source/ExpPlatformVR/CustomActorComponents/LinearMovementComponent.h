#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "LinearMovementComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class EXPPLATFORMVR_API ULinearMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	ULinearMovementComponent();

	// Called when the game starts
	virtual void BeginPlay() override;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;	

	void ResetComponent();

	UPROPERTY()
		float timeCurrent;
	UPROPERTY()
		FVector positionA;
	UPROPERTY()
		FVector positionB;
	UPROPERTY()
		FVector positionCurrent;
	UPROPERTY()
		bool arrived;
};
