#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SinusoidalMovementComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class EXPPLATFORMVR_API USinusoidalMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	USinusoidalMovementComponent();

	virtual void BeginPlay() override;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;	

	UPROPERTY()
		FVector randomPhase[5];
	UPROPERTY()
		FVector targetPosition;
	UPROPERTY()
		double frequencyArray[5];
	UPROPERTY()
		float timeStart;
	UPROPERTY()
		float timeCurrent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FVector SinusoidalTransform;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool active;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MovementRotation")
		float transformScale;
};
