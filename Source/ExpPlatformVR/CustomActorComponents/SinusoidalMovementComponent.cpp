#include "SinusoidalMovementComponent.h"
#include "CustomMath/CustomMath.h"
#include <Windows.h>

//UNREAL units are in cm, rather than OpenGL's m
#define FREQ_SCALE 1
#define TRANSFORM_SCALE 0.05//5 

USinusoidalMovementComponent::USinusoidalMovementComponent() : 
	frequencyArray{ 0.09*FREQ_SCALE, 0.165*FREQ_SCALE, 0.195*FREQ_SCALE, 0.375*FREQ_SCALE, 0.495*FREQ_SCALE }
{
	PrimaryComponentTick.bCanEverTick = true;

	for (int i = 0; i < 5; i++) {
		randomPhase[i].Set(
			RandFloat(2 * PI_F),
			RandFloat(2 * PI_F),
			RandFloat(2 * PI_F));
	}
	transformScale = 0.05f;
	targetPosition.Set(0, 0, 0);
}

// Called when the game starts
void USinusoidalMovementComponent::BeginPlay()
{
	Super::BeginPlay();
	timeStart = GetWorld()->GetRealTimeSeconds();
	timeCurrent = 0;
}

// Called every frame
void USinusoidalMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{	
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	timeCurrent = GetWorld()->GetRealTimeSeconds() - timeStart;

	targetPosition.Set(0, 0, 0);
	for (int i = 0; i < 5; i++) {
		targetPosition.X += (float)(sin(timeCurrent * 2 * PI_F * frequencyArray[i] + randomPhase[i].X)) * transformScale;
		targetPosition.Y += (float)(sin(timeCurrent * 2 * PI_F * frequencyArray[i] + randomPhase[i].Y)) * transformScale;
		targetPosition.Z += (float)(sin(timeCurrent * 2 * PI_F * frequencyArray[i] + randomPhase[i].Z)) * transformScale;
	}

	AActor* Parent = GetOwner();
	if (Parent && active)
	{
		Parent->SetActorLocation(Parent->GetActorLocation() + targetPosition);
	}
	FRotator temp;
}