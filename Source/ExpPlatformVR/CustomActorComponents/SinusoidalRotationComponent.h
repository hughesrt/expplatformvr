#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SinusoidalRotationComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class EXPPLATFORMVR_API USinusoidalRotationComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	USinusoidalRotationComponent();

	virtual void BeginPlay() override;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY()
		FVector randomPhase[5];
	UPROPERTY()
		FRotator targetRotation;
	UPROPERTY()
		double frequencyArray[5];
	UPROPERTY()
		float timeStart;
	UPROPERTY()
		float timeCurrent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MovementRotation")
		FRotator SinusoidalRotation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MovementRotation")
		bool active;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MovementRotation")
		float transformScale;
};
