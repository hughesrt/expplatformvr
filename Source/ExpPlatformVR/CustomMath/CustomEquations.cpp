#include "CustomEquations.h"
#include <stdarg.h>
#include <string.h>

#define kPsLineLength	512

/*
* Print Error
*/
void PsReportError(const char *txt, ...)
{
	va_list	ap;
	char	str[256];

	va_start(ap, txt);
	vsprintf_s(str, txt, ap);
	va_end(ap);
	fprintf(stdout, "%s\n", str);
}

/*
* Display file error
*/
static void PsFileError(char *error, char *file)
{
	PsReportError("Error %s file - %s.", error, file);
}

/*
* Read
*
*	Returns true if successful
*	Returns false otherwise
*/
static int PsReadSetting(char *str, int line)
{
	int				rs;
	sLDTrialRecord	*ir;

	if (gLDNrTrials < kLDMaxNrTrials) {
		ir = &gLDTrialArray[gLDNrTrials];
		rs = sscanf_s(str, gPsReadFormat, kLDRead(ir));
		ir->decEmb = -9999;
		ir->decAg = -9999;
		ir->decPres = -9999;
		if (rs != kLDInputItems) {
			PsReportError("Error in line %d of input file.", line);
			return 0;
		}
		gLDNrTrials++;
	}
	else {
		PsReportError("Error: too many trials in input");
		return 0;
	}

	return 1;
}

/*
* Parse a line from file
*/
static int PsParseOneLine(char *str, int line)
{
	if (*str == '#') {
		return 1;
	}
	else {
		return PsReadSetting(str, line);
	}
}

/*
* Read data set file
*
*	Returns false on an error
*	Returns true otherwise
*/
int PsReadDataSetFile(void)
{
	FILE	*ifp;
	int		rs;
	int		line;
	char	str[128];
	errno_t err;

	gLDNrTrials = 0;
	line = 1;

	err = fopen_s(&ifp, gLDInputFileName, "r"); // Open info file
	fprintf(stdout, "inpute file: %s err: %i\n", gLDInputFileName, err);

	if (err == 0)
	{
		do {
			if (fgets(str, 128, ifp)) 
			{
				rs = PsParseOneLine(str, line);
			}
			else 
			{
				rs = EOF;
			}
			line++;
		} while (rs == 1);

		fclose(ifp);
		if ((gLDNrTrials == 0) && (rs == EOF)) {
			PsReportError("Error: no trial information found");
			rs = 0;
		}
		else if (rs == EOF)
		{
			rs = 1;
		}
	}
	else 
	{
		PsFileError("opening", gLDInputFileName);
		rs = 0;
	}
	return rs;
}

/*
* Open output file append mode
*
*	Returns true if successful
*	Returns false otherwise
*
*	Trials are saved serially (Order intact)
*/
static int PsOpenOutputFile(void)
{
	if ((fopen_s(&gLDOpfTrial, gLDOutputFileName, "a")) != 0) {
		PsFileError("opening", gLDOutputFileName);
		return 0;
	}
	else {
		return 1;
	}
}

/*
* Close output file
*/
static void PsCloseOutputFile(void)
{
	if (fclose(gLDOpfTrial) == EOF) {		// If closing not ok ...
		PsFileError("closing", gLDOutputFileName);
	}
	printf("Close OutputFile\n");
}

/*
* Write results
*
*	Returns true if successful
*	Returns false otherwise
*/
int PsSaver(sLDTrialRecord *im)
{
	if (PsOpenOutputFile() == 1) {
		fprintf(stdout, "saving the trial information\n");
		if (fprintf(gLDOpfTrial, gPsWriteFormat, kLDWrite(im)) == EOF) {
			PsFileError("writing", gLDOutputFileName);
			return 0;
		}
		if (fprintf(gLDOpfTrial, "\n") == EOF) {
			PsFileError("writing", gLDOutputFileName);
			return 0;
		}
		PsCloseOutputFile();
	}
	else {
		return 0;
	}
	return 1;
}

/*
* Open output file append mode
*
*	Returns true if successful
*	Returns false otherwise
*
*	Trials are saved in their corresponding series meaning that each staircase series 
*	is saved sequentially (this function is called at NEAT CLOSURE at end of session)
*	WARNING: this data will not be saved upon premature closing or fast exit of program!!!
*/
int LDTrack_OpenOutputFile(void)
{
	if ((fopen_s(&gLDOpfTrack, gLDTrackFileName, "a")) == 0) {
		return 1;
	}
	else {
		PsFileError("opening", gLDTrackFileName);
		return 0;
	}
}

/*
* Close output file
*/
void LDTrack_CloseOutputFile(void)
{
	if (fclose(gLDOpfTrack) == EOF) {		// If closing not ok ...
		PsFileError("closing", gLDTrackFileName);
	}
	printf("Close OutputFile\n");
}

/*
* Save Staircase Series
*/
int LDTrack_Saver(void)
{
	int					i;
	sLDTrackRecord		*im;
	static char			str[] = "#file = %s ---\n";

	sprintf_s(gLDTrackFileName, "data/track_%s_%i.txt", gLDSubjectName, gLDTrialIndex);

	if (LDTrack_OpenOutputFile() == 1) {
		if (fprintf(gLDOpfTrack, str, gLDInputFileName) == EOF) {
			PsFileError("writing", gLDTrackFileName);
			return 0;
		}
		for (i = 0; i < gLDNrSamp; i++) {
			im = &gLDHandPosArray[i];
			if (fprintf(gLDOpfTrack, gLDWriteFormatTrack, kLDWriteTrack(im)) == EOF) {
				PsFileError("writing", gLDTrackFileName);
				return 0;
			}
			if (fprintf(gLDOpfTrack, "\n") == EOF) {
				PsFileError("writing", gLDTrackFileName);
				return 0;
			}
		}
		LDTrack_CloseOutputFile();
	}
	else {
		return 0;
	}
	return 1;
}