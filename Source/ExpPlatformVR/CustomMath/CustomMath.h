#pragma once

const float  PI_F = 3.14159265358979f;

static float RandFloat(float max)
{
	return (max * (float)(rand()) / (float)(RAND_MAX));
}