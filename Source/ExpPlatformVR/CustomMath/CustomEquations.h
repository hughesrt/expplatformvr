#pragma once 

#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <math.h>

// End user alterable defines
#define kPsFNameLength		128
#define kPsSNameLength		32
#define kLDProgramName		"VR_Delay_Adaptation"

//	-------- formats for trial data ---------------
typedef struct {
	int		trialnr;	// the trialnumber in the sequence
	float	delay;		// the feedback delay in the trial
	DWORD	disptime;	// display time
	float	decEmb;		// decision on the embodiment scale
	float	decAg;		// decision op the agency scale
	float	decPres;	// decision on the presence scale
} sLDTrialRecord;

char gPsReadFormat[] =  "%d  %f  %u\n";
char gPsWriteFormat[] = "%d\t%f\t%u\t%f\t%f\t%f";

#define kLDInputItems	3

#define kLDRead(a)		&a->trialnr, \
						&a->delay, \
						&a->disptime

#define kLDWrite(a)		a->trialnr, \
						a->delay, \
						a->disptime, \
						a->decEmb, \
						a->decAg, \
						a->decPres

//	-------- formats for trial data ---------------
typedef struct {
	float	HandPos_X;	// Hand position x
	float	HandPos_Y;	// Hand position y
	float	HandPos_Z;	// Hand position z
	float	HandRot_X;	// Hand orientation x
	float	HandRot_Y;	// Hand orientation y
	float	HandRot_Z;	// Hand orientation z
	float	HandRot_W;	// Hand orientation w
	DWORD	curTime;	// the current timestamp (Here DWORD, have created array of floats to store this rather than a record)
	float	Target_X;	// target position in X
	float	Target_Y;	// target position in Y
	float	Target_Z;	// target position in Z
	int		showTime;	// nr of samples back for which feedback is displayed
} sLDTrackRecord;

char gLDWriteFormatTrack[] = "%f\t%f\t%f\t%f\t%f\t%f\t%f\t%d\t%f\t%f\t%f\t%d";

#define kLDWriteTrack(a)	a->HandPos_X, \
							a->HandPos_Y, \
							a->HandPos_Z, \
							a->HandRot_X, \
							a->HandRot_Y, \
							a->HandRot_Z, \
							a->HandRot_W, \
							a->curTime, \
							a->Target_X, \
							a->Target_Y, \
							a->Target_Z, \
							a->showTime

// -------- end Data Formats ----------------
#define	kLDMaxNrSamples 3000
#define	kLDMaxNrTrials	60

// for trial records
sLDTrialRecord	gLDTrialArray[kLDMaxNrTrials];
sLDTrialRecord	*gLDCurTrial;
int				gLDTrialIndex, gLDNrTrials;
char				gLDOutputFileName[kPsFNameLength];
FILE				*gLDOpfTrial;

// for track records
sLDTrackRecord	gLDHandPosArray[kLDMaxNrSamples];
char				gLDTrackFileName[kPsFNameLength];
FILE				*gLDOpfTrack;
int				gLDNrSamp;

// ------------ Furthers Structs etc --------
enum {
	kLDShowTrialStart = 0,
	kPsShowTrial,
	kLDGetResponses
}; 

// ----- Definitions of further global variables --------
char gLDInputFileName[kPsFNameLength];
char gLDSubjectName[kPsSNameLength];
bool gLDShowShadowTargets;

#ifndef __OGL_PROT__
#define __OGL_PROT__
	//From OLG-FILE.C
	void	PsReportError(const char *txt, ...);
	int		PsReadDataSetFile(void);
	int		PsSaver(sLDTrialRecord *im);
	//int	PsOpenOutputFile(void);
	//void	PsCloseOutputFile(void);
	int		LDTrack_Saver(void);
#endif //__OGL_PROT__