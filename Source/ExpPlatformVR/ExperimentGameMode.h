#pragma once

#include "GameFramework/GameMode.h"
#include "CustomActors/GaussDotsActor.h"
#include "ExperimentGameMode.generated.h"

/**
 * 
 */
UCLASS()
class EXPPLATFORMVR_API AExperimentGameMode : public AGameMode
{
	GENERATED_BODY()
	
		virtual void BeginPlay() override;

	UPROPERTY()
		AGaussDotsActor* SpawnedGaussDotsActor;

	UFUNCTION()
		void DestroyActorFunction();
};
