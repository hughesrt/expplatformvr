#include "GaussDotsActor.h"
#include "ExpPlatformVR.h"
#include <random>

#define N_DOTS 1000

// Sets default values
AGaussDotsActor::AGaussDotsActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UInstancedStaticMeshComponent>("BaseMeshComponent");

	static ConstructorHelpers::FObjectFinder<UMaterial> CursorMaterial(TEXT("Material'/Game/Materials/CursorMaterial.CursorMaterial'"));
	if (CursorMaterial.Object != NULL)
	{
		cursor_material = (UMaterial*)CursorMaterial.Object;
	}
	static ConstructorHelpers::FObjectFinder<UMaterial> TargetMaterial(TEXT("Material'/Game/Materials/TargetMaterial.TargetMaterial'"));
	if (TargetMaterial.Object != NULL)
	{
		target_material = (UMaterial*)TargetMaterial.Object;
	}

	arrivedDest = false;

	//Create Random Distribution
	std::default_random_engine generator(std::random_device{}());
	std::normal_distribution<double> distribution(0.0, 1.0);
	spreadDots = sqrt(20);
	spheresFactor = 0.65f;
	nDots = N_DOTS*spheresFactor;

	for (int i = 0; i < nDots; i++)
	{
		positionArray.Add(
			spreadDots * FVector(distribution(generator),
								distribution(generator), 
								distribution(generator)));
		rotatedPositionArray.Add(positionArray[i]);
	}

	//Create Spheres
	UWorld* const World = GetWorld();
	if (World) {
		for (int i = 0; i < nDots; i++)
		{
			Dots.Add(World->SpawnActor<AProceduralSphereActor>(AProceduralSphereActor::StaticClass(), FTransform(positionArray[i])));
			Dots[i]->SetActorEnableCollision(false);
		}
	}
	CursorAngle = FVector(0.f, 0.f, 0.f);

	//SinusoidalMovement = CreateDefaultSubobject<USinusoidalMovementComponent>("SinusoidalMovement");
}

// Called when the game starts or when spawned
void AGaussDotsActor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AGaussDotsActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	for (int i = 0; i < nDots; i++)
	{
		if(Dots[i]->lifespan < 1)
		{ 
			FVector newPos = spreadDots * FVector(
				GetPointDistribution(0.0, 1.0),
				GetPointDistribution(0.0, 1.0),
				GetPointDistribution(0.0, 1.0));
			Dots[i]->lifespan = std::rand() % 9 + 3;
			positionArray[i] = newPos;

			//if (SinusoidalMovement->active)
			//{
			//	positionArray[i].Y += SinusoidalMovement->SinusoidalTransform.Z;
			//}
		}
		else {
			Dots[i]->lifespan -= 1;
		}
	}
	//Now Rotate By Cursor Angle
	for (int i = 0; i < nDots; i++)
	{
		FVector newPos = positionArray[i];
		newPos = newPos.RotateAngleAxis(CursorAngle.X, FVector(1.0f, 0.0f, 0.0f));
		newPos = newPos.RotateAngleAxis(CursorAngle.Y, FVector(0.0f, 1.0f, 0.0f));
		newPos = newPos.RotateAngleAxis(CursorAngle.Z, FVector(0.0f, 0.0f, 1.0f));
		rotatedPositionArray[i] = newPos;
	}
	for (int i = 0; i < nDots; i++)
	{
		Dots[i]->SetActorLocation(GetActorLocation() + rotatedPositionArray[i]);
	}
}

void AGaussDotsActor::ActivateTargetReset()
{
	//Create Random Distribution
	for (int i = 0; i < nDots; i++)
	{
		positionArray[i] = FVector(
			spreadDots * FVector(
				GetPointDistribution(0.0, 1.0),
				GetPointDistribution(0.0, 1.0),
				GetPointDistribution(0.0, 1.0)));
	}

	for (int i = 0; i < nDots; i++)
	{
		Dots[i]->SetActorLocation(positionArray[i]);
	}
}

void AGaussDotsActor::SetTargetMoveTransform(FVector t) {
	SetActorLocation( GetActorLocation() + t);
}

void AGaussDotsActor::SetTargetHidden(bool hide)
{
	for (int i = 0; i < nDots; i++)
	{
		Dots[i]->SetActorHiddenInGame(hide);
	}
}

bool AGaussDotsActor::GetTargetArrived()
{
	return arrivedDest;
}

void AGaussDotsActor::SetSpreadToTarget()
{
	//Create Spheres
	UWorld* const World = GetWorld();
	if (World) {
		for (int i = 0; i < nDots; i++)
		{
			Dots[i]->Destroy();
		}
		Dots.Add(World->SpawnActor<AProceduralSphereActor>(AProceduralSphereActor::StaticClass(), FTransform(GetActorLocation())));
		positionArray[0] = FVector(0, 0, 0);
		Dots[0]->SetActorEnableCollision(false);
	}

}

float AGaussDotsActor::GetPointDistribution(float m, float d) 
{
	std::default_random_engine generator(std::random_device{}());
	std::normal_distribution<double> distribution(m, d);

	return distribution(generator);
}

void AGaussDotsActor::SetCursorMaterial()
{
	for (int i = 0; i < nDots; i++)
	{
		Dots[i]->mesh->SetMaterial(0, cursor_material);
	}
}

void AGaussDotsActor::SetTargetMaterial()
{
	for (int i = 0; i < nDots; i++)
	{
		Dots[i]->mesh->SetMaterial(0, target_material);
	}
}

void AGaussDotsActor::DecreaseNumberOfSpheres()
{
	spheresFactor = FMath::Clamp(spheresFactor - 0.1f, 0.0f, 1.0f);
	nDots = N_DOTS*spheresFactor;

	positionArray.Empty();
	rotatedPositionArray.Empty();
	Dots.Empty();

	for (int i = 0; i < nDots; i++)
	{
		positionArray.Add(
			spreadDots * FVector(
				GetPointDistribution(0.0, 1.0),
				GetPointDistribution(0.0, 1.0),
				GetPointDistribution(0.0, 1.0)));
		rotatedPositionArray.Add(positionArray[i]);
	}

	//Create Spheres
	UWorld* const World = GetWorld();
	if (World) {
		for (int i = 0; i < nDots; i++)
		{
			Dots.Add(World->SpawnActor<AProceduralSphereActor>(AProceduralSphereActor::StaticClass(), FTransform(positionArray[i])));
			Dots[i]->SetActorEnableCollision(false);
		}
	}

	SetCursorMaterial();
}

void AGaussDotsActor::IncreaseNumberOfSpheres()
{
	spheresFactor = FMath::Clamp(spheresFactor + 0.1f, 0.0f, 1.0f);
	nDots = N_DOTS*spheresFactor;

	positionArray.Empty();
	rotatedPositionArray.Empty();
	Dots.Empty();

	for (int i = 0; i < nDots; i++)
	{
		positionArray.Add(
			spreadDots * FVector(
				GetPointDistribution(0.0, 1.0),
				GetPointDistribution(0.0, 1.0),
				GetPointDistribution(0.0, 1.0)));
		rotatedPositionArray.Add(positionArray[i]);
	}

	//Create Spheres
	UWorld* const World = GetWorld();
	if (World) {
		for (int i = 0; i < nDots; i++)
		{
			Dots.Add(World->SpawnActor<AProceduralSphereActor>(AProceduralSphereActor::StaticClass(), FTransform(positionArray[i])));
			Dots[i]->SetActorEnableCollision(false);
		}
	}

	SetCursorMaterial();
}