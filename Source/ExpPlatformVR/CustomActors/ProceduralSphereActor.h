#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
//#include "CustomActorComponents/SinusoidalMovementComponent.h"
#include "ProceduralSphereActor.generated.h"

UCLASS()
class EXPPLATFORMVR_API AProceduralSphereActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProceduralSphereActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY()
		int32 lifespan;
	UPROPERTY()
		TArray<int32> triangles;
	UPROPERTY()
		TArray<FVector> vertices;
	UPROPERTY()
		TArray<FVector> normals;
	UPROPERTY()
		TArray<FVector2D> texcoords;
	UPROPERTY()
		TArray<FProcMeshTangent> tangents;
	UPROPERTY()
		TArray<FLinearColor> vertexColors;

	UPROPERTY(VisibleAnywhere, Category = Materials)
		UProceduralMeshComponent* mesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Materials)
		UMaterial* material_target;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Materials)
		UMaterial* material_cursor;

	UFUNCTION(BlueprintCallable, Category = "Target")
		void SetTargetMaterial();

	UFUNCTION(BlueprintCallable, Category = "Target")
		void SetCursorMaterial();

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	//	USinusoidalMovementComponent* SphereMovement;
};