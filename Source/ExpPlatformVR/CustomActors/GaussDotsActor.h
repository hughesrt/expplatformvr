#pragma once

#include "GameFramework/Actor.h"
#include "Components/InstancedStaticMeshComponent.h"
//#include "CustomActorComponents/SinusoidalMovementComponent.h"
#include "CustomActorComponents/LinearMovementComponent.h"
#include "CustomActors/ProceduralSphereActor.h"
#include "GaussDotsActor.generated.h"

UCLASS()
class EXPPLATFORMVR_API AGaussDotsActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGaussDotsActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable, Category = "Target")
	void ActivateTargetReset();

	UFUNCTION(BlueprintCallable, Category = "Target")
	void SetTargetHidden(bool hide);
	
	UFUNCTION(BlueprintPure, Category = "Target")
	bool GetTargetArrived();

	UFUNCTION(BlueprintCallable, Category = "Target")
	void SetTargetMoveTransform(FVector t);

	UFUNCTION(BlueprintCallable, Category = "Target")
	void SetSpreadToTarget();

	UFUNCTION(BlueprintCallable, Category = "Target")
	float GetPointDistribution(float m, float d);

	UFUNCTION(BlueprintCallable, Category = "Target")
	void SetCursorMaterial();

	UFUNCTION(BlueprintCallable, Category = "Target")
	void SetTargetMaterial();

	UFUNCTION(BlueprintCallable, Category = "Target")
	void IncreaseNumberOfSpheres();

	UFUNCTION(BlueprintCallable, Category = "Target")
	void DecreaseNumberOfSpheres();

	//Properties
	UPROPERTY()
		UStaticMeshComponent* Mesh;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target")
		FVector CursorAngle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target")
		TArray<AProceduralSphereActor*> Dots;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	//	USinusoidalMovementComponent* SinusoidalMovement;

	//Dimensions & Properties of Dot Cloud
	UPROPERTY()
		TArray<FVector> positionArray;
		TArray<FVector> rotatedPositionArray;
	UPROPERTY()
		TArray<FVector> colorArray;
	UPROPERTY()
		TArray<float> sizeArray;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CloudProperties")
		float spreadDots;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CloudProperties")
		float spheresFactor;

	UPROPERTY()
		int nDots;

	//Logic Controllers
	UPROPERTY()
		bool arrivedDest;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Materials)
		UMaterial* cursor_material;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Materials)
		UMaterial* target_material;

	UPROPERTY()
		bool cursor;
};
