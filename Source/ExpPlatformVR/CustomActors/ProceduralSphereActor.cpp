#include "ProceduralSphereActor.h"
#include "ExpPlatformVR.h"
#include "CustomMath/CustomMath.h"

AProceduralSphereActor::AProceduralSphereActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	static ConstructorHelpers::FObjectFinder<UMaterial> MaterialTarget(TEXT("Material'/Game/Materials/TargetMaterial.TargetMaterial'"));
	if (MaterialTarget.Object != NULL)
	{
		material_target = (UMaterial*)MaterialTarget.Object;
	}

	static ConstructorHelpers::FObjectFinder<UMaterial> MaterialCursor(TEXT("Material'/Game/Materials/CursorMaterial.CursorMaterial'"));
	if (MaterialCursor.Object != NULL)
	{
		material_cursor = (UMaterial*)MaterialCursor.Object;
	}

	
	bool collision = true;

	float radius = 0.25;
	int rings = 24;
	int sectors = 48;
	lifespan = std::rand() % 9 + 3;

	float const R = 1.f / (float)(rings - 1);
	float const S = 1.f / (float)(sectors - 1);
	int r, s;

	for (r = 0; r < rings; r++) for (s = 0; s < sectors; s++)
	{
		float  y = FMath::Sin((-PI_F / 2) + PI_F * r * R);
		float  x = FMath::Cos(2 * PI_F * s * S) * FMath::Sin(PI_F * r * R);
		float  z = FMath::Sin(2 * PI_F * s * S) * FMath::Sin(PI_F * r * R);

		vertices.Add(FVector(x * radius, y * radius, z * radius));
		normals.Add(FVector(x, y, z));
		texcoords.Add(FVector2D(s*S, r*R));
		tangents.Add(FProcMeshTangent(0, 1, 0));
		vertexColors.Add(FLinearColor(1.0, 0, 0, 1.0));
	}

	for (r = 0; r < rings - 1; r++) for (s = 0; s < sectors - 1; s++) {
		triangles.Add(s + sectors*r);
		triangles.Add(s + 1 + sectors*r);
		triangles.Add(sectors + s + sectors*r);
		triangles.Add(sectors + s + 1 + sectors*r);
		triangles.Add(sectors + s + sectors*r);
		triangles.Add(s + 1 + sectors*r);
	}

	mesh->CreateMeshSection_LinearColor(0, vertices, triangles, normals, texcoords, vertexColors, tangents, collision);
	mesh->SetMaterial(0, material_target);

	//SphereMovement = CreateDefaultSubobject<USinusoidalMovementComponent>("SphereMovement");
}

void AProceduralSphereActor::BeginPlay()
{
	Super::BeginPlay();
}

void AProceduralSphereActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/*if (SphereMovement->active)
	{
		SetActorLocation(GetActorLocation() + SphereMovement->SinusoidalTransform);
	}*/
}

void AProceduralSphereActor::SetTargetMaterial()
{
	mesh->SetMaterial(0, material_target);
}

void AProceduralSphereActor::SetCursorMaterial()
{
	mesh->SetMaterial(0, material_cursor);
}