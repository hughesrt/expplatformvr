#include "ExperimentGameMode.h"

#include "ExpPlatformVR.h"
#include "CustomActors/GaussDotsActor.h"


void AExperimentGameMode::BeginPlay()
{
	Super::BeginPlay();
	FTransform SpawnLocation;
	GEngine->AddOnScreenDebugMessage(-1, -1, FColor::Red, TEXT("Actor Spawning"));
	//SpawnedGaussDotsActor = GetWorld()->SpawnActor<AGaussDotsActor>(AGaussDotsActor::StaticClass(), SpawnLocation);

	//FTimerHandle Timer;
	//GetWorldTimerManager().SetTimer(Timer, this, &AExperimentGameMode::DestroyActorFunction, 20);
}

void AExperimentGameMode::DestroyActorFunction()
{
	if (SpawnedGaussDotsActor != nullptr)
	{
		SpawnedGaussDotsActor->Destroy();
	}
}